"""This python script is used to calculate miRNA coverage
in the downloaded 1000 Genomes GRCh38 low coverage data (.sam)
in parallel

This code is used in the following format

    python path/to/miRNAReadCoverage_parallel.py

"""

import multiprocessing as mp
import logging
from referencemiRNA import *
import os
from os import listdir
import pandas as pd
import platform
import matplotlib.pyplot as pl

if platform.system() == 'Darwin':  # Mac for testing
    # mirna file location
    dir_mirna = '/Users/yafeix/Work/Projects/1XGenome/Materials/hsa_miRNA_without_head.gff3'
    # data folder
    dir_data = '/Users/yafeix/Work/Projects/1XGenome/Data'
    # output folder
    dir_output = '/Users/yafeix/Work/Projects/1XGenome/Analysis/mirna_coverage'
else:  # Fedora for execution
    dir_mirna = '/home/yafei/Projects/1XGenome/Materials/hsa_miRNA_without_head.gff3'
    dir_data = '/run/media/yafei/My Book Duo/1XGenome/data'
    dir_output = '/home/yafei/Projects/1XGenome/Analysis/mirna_coverage'

# create files for saving the coverage of each nt in each mirna
# Head of the file: mirna sample country coverage
filename = 'Individual_coverage_mirna.tsv'
fwrite2 = os.path.join(dir_output, filename)
f2write = open(fwrite2, 'w')
head = 'mirnaID\tchr\tsample\tcountry\tcoverage\n'
f2write.write(head)

list_country = ['GWD', 'ESN', 'JPT', 'CHS', 'LWK', 'FIN', 'CDX', 'BEB', 'PEL', 'KHV']


def coverage_all(mirna, dire):
    """
    :type dir: directory of genome data
    """
    global dir_data, dir_output, ave_coverage, only_value
    if set(listdir(dire)) <= set(list_country):
        # get the country code
        for country_code in listdir(dire):
            mirna.country = country_code

            # the results of average coverage of each mirna in each sample of each country will be saved in f1write
            f1write_name = 'mirna_coverage_' + mirna.id + '.csv'
            f1write_path = os.path.join(dir_output, f1write_name)
            # get the country folder path
            dir_country = os.path.join(dir_data, country_code)
            # go to the country folder
            coverage_per_country(mirna, dir_country)
            logging.info('finished process in country %s for %s', mirna.country, mirna.id)

        ave_coverage.to_csv(f1write_path, sep='\t')
        save_plot(mirna)
        logging.info('results of coverage and histograms have been saved for %s', mirna.id)


def save_plot(mirna):
    global dir_output, only_value
    fig_name = dir_output + '/figures/histograms/mirna_coverage_histgram_' + mirna.id + '.png'
    fig = pl.hist(only_value, bins=50)
    pl.title('mirna average coverage')
    pl.xlabel('average coverage')
    pl.ylabel('Frequency')
    pl.savefig(fig_name)


def coverage_per_country(mirna, country):
    logging.info('start to process in country %s for %s', mirna.country, mirna.id)
    # obtain all the subdirectories named by sample ID
    # get all the samples ID
    global ave_coverage, only_value
    ave_coverage_temp = pd.DataFrame()
    list_samples = listdir(country)
    sample_folders = [os.path.join(country, x) for x in list_samples]
    # add the sample column to the dataframe
    ave_coverage_temp[mirna.country] = list_samples
    # average coverages of one mirna in one country
    ave_country = []

    for f in sample_folders:
        mirna.sample = os.path.basename(f)
        coverage_per_sample(mirna, f)
        ave_country.append(mirna.ave_coverage)
    # add the average coverage column to the dataframe
    ave_coverage_temp['ave_coverage'] = ave_country
    only_value.append(ave_country)
    ave_coverage = pd.concat([ave_coverage, ave_coverage_temp], axis=1)


def coverage_per_sample(mirna, fsample):
    # print(3)
    # folders of chromosomes
    chromo_folder = os.path.join(fsample, mirna.chromo)
    coverage_per_chromo(mirna, chromo_folder)


def coverage_per_chromo(mirna, chromo):
    """
    This function searches for the corresponding .sam file in the corresponding chromosome folder
    in one sample
    :param mirna: reference mirna object containing mirna info
    :param chromo: the path of the corresponding chromosome folder in one sample of the mirna
    """
    global f2write
    # print(4)
    sam_files = listdir(chromo)
    for file in sam_files:
        # find the corresponding download
        ind1 = file.find('MIMAT')
        ind2 = file.rfind('_')
        mirnaID = file[ind1:ind2]
        if mirna.id == mirnaID:
            # print('process sam file')
            file_path = os.path.join(chromo, file)
            logging.info('calculating coverage in %s corresponded sam file in sample %s', mirna.id, mirna.sample)
            mirna.coverage = coverage_per_sam(mirna, file_path)
            mirna.ave_coverage = sum(mirna.coverage) / len(mirna.coverage)
            row = mirna.id + '\t' + mirna.chromo + '\t' + mirna.sample + '\t' + mirna.country + '\t' + str(
                mirna.coverage) + '\n'
            f2write.write(row)
            logging.info('coverage of %s in %s is saved', mirna.id, mirna.sample)
        else:
            continue


def coverage_per_sam(mirna, file):
    """
    This function is for calculating the coverage of the mirna in the same file
    :param mirna: reference mirna object containing mirna info
    :param file: the .sam file corresponding to the mirna in one sample
    :return: the coverage of each nt in a vector
    """
    coverage = [0] * (mirna.end - mirna.start + 1)  # coverage for each nt in mirna
    with open(file, 'r') as fsam:
        for read in fsam:
            genome_read = GenomeReads(start=int(read.split('\t')[3]), align=read.split('\t')[9], length=101)
            # check if there is any read who length is not 101
            if len(genome_read.align) != 101:
                # print(read.split('\t')[0], len(genome_read.align))
                genome_read.length = len(genome_read.align)
            coverage = count_per_read(coverage, mirna, genome_read)
    return coverage


def count_per_read(count, mirna, read):
    """
    :type mirna: ReferencemiRNA object
    :type count: the coverage counting array
    :type read: GenomeReads object
    """
    for ind, val in enumerate(count):
        pos_ind = mirna.start + ind  # the position of the nt to be processed
        if read.start <= pos_ind <= (read.start + read.length - 1):  # if it is in the range of the read
            count[ind] = val + 1
    return count


def process_mirna(line_mirna):
    # create a mirna object
    global ave_coverage, only_value, calculated_mirna
    mirnaInfo = line_mirna.split(' ')[3]
    # obtain mirna ID
    ind = mirnaInfo.find('=')
    ind2 = mirnaInfo.find(';')
    # create mirna object with current reference mirna read
    ref_mirna = ReferencemiRNA(id=mirnaInfo[ind + 1: ind2], chromo='chr' + line_mirna.split(' ')[0],
                               start=int(line_mirna.split(' ')[1]), end=int(line_mirna.split(' ')[2]),
                               coverage=[], ave_coverage=0, sample='', country='')
    # calculate the coverage of each mirna in downloaded genome data
    print(ref_mirna.id)

    logging.info('start to process mirna %s', ref_mirna.id)

    ave_coverage = pd.DataFrame()
    only_value = []
    coverage_all(ref_mirna, dir_data)

    logging.info('finished of processing mirna %s', ref_mirna.id)
    # delete the mirna object
    del ref_mirna, ave_coverage, only_value


logging.basicConfig(filename=dir_output + '/log/mirnacoverage_para.log', level=logging.INFO)
# read mirna file
with open(dir_mirna, 'r') as fmiRNA:
    logging.info('open reference mirna file')
    pool = mp.Pool(processes=8)
    [pool.apply(process_mirna, args=(line,)) for line in fmiRNA.readline()]
    logging.info('all mirna have been processed')
#fmiRNA.close()
f2write.close()
