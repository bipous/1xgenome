
dir_mirna = '/Users/yafeix/Work/Projects/1XGenome/Materials/hsa_miRNA_without_head.gff3'
fwrite2 = '/Users/yafeix/Work/Projects/1XGenome/Materials/hsa_miRNA_ID_name.txt'
f2write = open(fwrite2, 'w')

with open(dir_mirna, 'r') as fmiRNA:
    for line in fmiRNA.readlines():
        mirnaInfo = line.split(' ')[3]
        # obtain mirna ID
        ind = mirnaInfo.find('=')
        ind2 = mirnaInfo.find(';')
        id = mirnaInfo[ind + 1: ind2]
        ind = mirnaInfo.rfind('hsa-miR')
        ind2 = mirnaInfo.rfind(';')
        name = mirnaInfo[ind: ind2]
        miRname = id + "_" + name
        f2write.write(miRname+'\n')

f2write.close()
