
dir_mirna = '/Users/yafeix/Work/Projects/1XGenome/Materials/hsa_miRNA_without_head.gff3'

dir_processed_mirna = '/Users/yafeix/Work/Projects/1XGenome/Materials/processed_mirna_2.txt'

file2write = '/Users/yafeix/Work/Projects/1XGenome/Materials/hsa_miRNA_without_head_break.gff3'
f2write = open(file2write, 'w')

list_all_mirna = []

list_processed_mirna = []

with open(dir_mirna, 'r') as fmiRNA:
    for line in fmiRNA.readlines():
        mirnaInfo = line.split(' ')[3]
        # obtain mirna ID
        ind = mirnaInfo.find('=')
        ind2 = mirnaInfo.find(';')
        list_all_mirna.append(mirnaInfo[ind + 1: ind2])
print(list_all_mirna)

with open(dir_processed_mirna,'r') as fmiRNA:
    for line in fmiRNA.readlines():
        list_processed_mirna.append(line.rstrip('\n'))
print(list_processed_mirna)

list_unprocessed_mirna = [i for i, item in enumerate(list_all_mirna) if item not in set(list_processed_mirna)]
print(list_unprocessed_mirna)

fmiRNA = open(dir_mirna, 'r')
lines = fmiRNA.readlines()
for index in list_unprocessed_mirna:
    f2write.write(lines[index])

f2write.close()
fmiRNA.close()