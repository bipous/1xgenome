#!/usr/bin/env bash

file="/home/yafei/Projects/1XGenome/Materials/hsa_miRNA_ID_name.txt"
while IFS= read -r findex
do
	printf $findex
	find /media/MyBookDuo/1XGenome/data -name *$findex'.sam'  >> "/home/yafei/Projects/1XGenome/Materials/dir_index_sam_miRNA/dir_index_"$findex".txt"
	find /media/MyBookDuo/1XGenome/bam_data -name *$findex'.bam'  >> "/home/yafei/Projects/1XGenome/Materials/dir_index_bam_miRNA/dir_index_"$findex".txt"
done <"$file"

