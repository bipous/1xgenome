"""This python script is used to calculate miRNA coverage
in the downloaded 1000 Genomes GRCh38 low coverage data (.sam)
in parallel

This code is used in the following format

    python path/to/miRNAReadCoverage_parallel.py

"""

import multiprocessing as mp
import logging
from referencemiRNA import *
import os, glob
import pysam
import numpy as np
import pandas as pd
import platform
import time

if platform.system() == 'Darwin':  # Mac for testing
    # mirna file location
    dir_mirna = '/Users/yafeix/Work/Projects/1XGenome/Materials/hsa_miRNA_without_head.gff3'
    # data folder
    dir_data = '/Users/yafeix/Work/Projects/1XGenome/Data'
    # output folder
    dir_output = '/Users/yafeix/Work/Projects/1XGenome/Analysis/mirna_coverage'
else:  # Fedora for execution
    dir_mirna = '/home/yafei/Projects/1XGenome/Materials/hsa_miRNA_without_head.gff3'
    dir_bam_data = '/media/MyBookDuo/1XGenome/bam_data'
    dir_sam_data = '/media/MyBookDuo/1XGenome/data'
    dir_index_bam = '/home/yafei/Projects/1XGenome/Materials/dir_index_bam_miRNA'
    dir_output = '/home/yafei/Projects/1XGenome/Analysis/mirna_coverage'

list_country = ['GWD', 'ESN', 'JPT', 'CHS', 'LWK', 'FIN', 'CDX', 'BEB', 'PEL', 'KHV']
Coverage_name = ['Coverage_'+x for x in list_country]

logging.basicConfig(filename=dir_output + '/log/mirnacoverage_para.log', level=logging.INFO)


def process_mirna(line_mirna):
    # create a mirna object
    mirnaInfo = line_mirna.split(' ')[3]
    # obtain mirna ID
    ind = mirnaInfo.find('=')
    ind2 = mirnaInfo.find(';')
    miRid = mirnaInfo[ind + 1: ind2]
    # get miRNA name, let- miR were not considered (mistake)
    ind = mirnaInfo.rfind('hsa-miR')
    ind2 = mirnaInfo.rfind(';')
    miRname = miRid + "_" + mirnaInfo[ind: ind2]
    # create mirna object with current reference mirna read
    ref_mirna = ReferencemiRNA(id=miRid, name=miRname, chromo='chr' + line_mirna.split(' ')[0],
                               start=int(line_mirna.split(' ')[1]), end=int(line_mirna.split(' ')[2]), coverage=pd.DataFrame())
    ref_mirna.create_dataFrames(Coverage_name)
    return ref_mirna


def coverage_per_bam(dir_bam, ref_RNA):
    bf = pysam.AlignmentFile(dir_bam, 'rb')
    start = ref_RNA.start - 1
    count_ACGT = bf.count_coverage(ref_RNA.chromo, start, ref_RNA.end, quality_threshold=0, read_callback='nofilter')
    count_A = np.array(count_ACGT[0])
    count_C = np.array(count_ACGT[1])
    count_G = np.array(count_ACGT[2])
    count_T = np.array(count_ACGT[3])
    count_pnt = count_A + count_C + count_G + count_T
    count_reads = bf.count()
    bf.close()
    return count_reads, count_pnt, np.average(count_pnt)


def get_index(dir_index, miRname):
    os.chdir(dir_index)
    fname = '*' + miRname + '.txt'
    findex = glob.glob(fname)
    if len(findex) != 1:
        raise ValueError('The ID is not unique to find the index file.')
    else:
        return findex[0]


def save_bam_by_fetch(dir_bam_in, dir_bam_out, region, start, end):
    bf = pysam.AlignmentFile(dir_bam_in, 'rb')
    with pysam.AlignmentFile(dir_bam_out, "wb", header=bf.header) as outbam:
        for r in bf.fetch(region, start, end):
            outbam.write(r)
    # create the index for the output bam file
    pysam.index(dir_bam_out)
    bf.close()


def process_index(dir_index, rna_name):
    ind = dir_index.rfind('/')
    fname = dir_index[ind+1:]
    country = fname[24:27]
    sample = fname[28:35]
    index_out = '/media/MyBookDuo/1XGenome/miR_only_bam_data/' + rna_name + '/'+ country + '/'+ fname
    return index_out, country, sample


def process_per_bam(file_index, RNA):
    # process the index info and give the bam output index
    dir_out, countryID, sampleID = process_index(file_index, RNA.name)
    # fetch and save the miRNA related reads
    save_bam_by_fetch(file_index, dir_out, RNA.chromo, RNA.start-1, RNA.end)
    # calculate the coverage
    count_read, count_per_nt, count_ave = coverage_per_bam(file_index, RNA)

    return countryID, sampleID, count_read, count_per_nt, count_ave


def process_result(results, mirna):
    global Coverage_name
    # get the results for the parallel processing pool
    for res in results:
        get_result = res.get()
        column_total = 'total_reads_'+get_result[0]
        column_coverage = 'coverage_pnt_'+get_result[0]
        column_ave = 'ave_coverage_'+get_result[0]
        exec('mirna.Coverage_'+get_result[0]+'=mirna.Coverage_'+get_result[0]+'.append({get_result[0]: get_result[1], column_total: get_result[2], '
                                             'column_coverage: get_result[3], '
                                             'column_ave: get_result[4]}, ignore_index=True)')

    # merge the results to one csv
    for name in Coverage_name:
        exec('mirna.coverage = pd.concat([mirna.coverage, mirna.' + name + '], axis=1)')
    # write the results to a csv file
    fwrite = dir_output + '/results/mirna_coverage_' + mirna.name + '.csv'
    mirna.coverage.to_csv(fwrite, sep='\t')


# read mirna file
with open(dir_mirna, 'r') as fmiRNA:
    logging.info('open reference mirna file')
    info_miRNA = fmiRNA.readlines()  # load reference miRNA info in the memory
for line in info_miRNA:
    start_time = time.time()
    ref_miRNA = process_mirna(line)
    logging.info('start to process mirna %s', ref_miRNA.id)
    print('start to process mirna:', ref_miRNA.id)

    logging.info('get the index file of all the bam files related with the reference miRNA: %s', ref_miRNA.id)
    # get the index file for the reference miRNA
    findex = get_index(dir_index_bam, ref_miRNA.name)

    result = []
    logging.info('process all the bam files related with %s', ref_miRNA.id)
    with open(findex, 'r') as fbam:
        pool = mp.Pool(processes=8)
        for index_bam in fbam.readlines(): # can be processed in parallel
            #logging.info('process bam %s', index_bam)
            result.append(pool.apply_async(process_per_bam, args=(index_bam[:-1], ref_miRNA,)))
        pool.close()
        pool.join()

    logging.info('get and merge the results to one data frame for %s', ref_miRNA.id)
    process_result(result, ref_miRNA)
    print('time to process'+ ref_miRNA.id +':'+str(time.time()-start_time)+' seconds')
    logging.info('finished of processing mirna %s', ref_miRNA.id)
    # delete the mirna object
    del ref_miRNA

logging.info('all mirna have been processed')

