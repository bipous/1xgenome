"""This python script is used to reshape the dataframes
of results of the coverage calculation for all miRs over
downloaded 1X Genome data

The original columns of the dataframes look like:

	GWD	ave_coverage_GWD	coverage_pnt_GWD	total_reads_GWD
	ESN	ave_coverage_ESN	coverage_pnt_ESN	total_reads_ESN
	JPT	ave_coverage_JPT	coverage_pnt_JPT	total_reads_JPT
	CHS	ave_coverage_CHS	coverage_pnt_CHS	total_reads_CHS
	LWK	ave_coverage_LWK	coverage_pnt_LWK	total_reads_LWK
	FIN	ave_coverage_FIN	coverage_pnt_FIN	total_reads_FIN
	CDX	ave_coverage_CDX	coverage_pnt_CDX	total_reads_CDX
	BEB	ave_coverage_BEB	coverage_pnt_BEB	total_reads_BEB
	PEL	ave_coverage_PEL	coverage_pnt_PEL	total_reads_PEL
	KHV	ave_coverage_KHV	coverage_pnt_KHV	total_reads_KHV

The reshaped columns of the dataframes are

    country sampleID ave_coverage coverage_pnt total_reads

This code is used in the following format

    python path/to/Reshape_DataFrame.py

"""

import pandas as pd
import os, glob
list_country = ['GWD', 'ESN', 'JPT', 'CHS', 'LWK', 'FIN', 'CDX', 'BEB', 'PEL', 'KHV']

dir_in = '/home/yafei/Projects/1XGenome/Analysis/mirna_coverage/results/'
dir_output = '/home/yafei/Projects/1XGenome/Analysis/mirna_coverage/ungrouped/'

os.chdir(dir_output)
findex = glob.glob('*.csv')
for fname in findex:
    df = pd.read_csv(dir_in+fname,sep='\t')
    df_new = pd.DataFrame(columns=['sampleID', 'ave_coverage', 'coverage_pnt', 'total_reads', 'country'])
    for country in list_country:
        df_temp = pd.DataFrame(columns=['sampleID', 'ave_coverage', 'coverage_pnt', 'total_reads', 'country'])
        df_temp['sampleID'] = df[country]
        df_temp['ave_coverage'] = df['ave_coverage_'+country]
        df_temp['coverage_pnt'] = df['coverage_pnt_'+country]
        df_temp['total_reads'] = df['total_reads_'+country]
        df_temp['country'] = country
        df_temp = df_temp.dropna(axis=0, how='any')
        df_new = df_new.append(df_temp)
        del(df_temp)
    fwrite = dir_output + 'ungrouped_' +fname
    df_new.to_csv(fwrite,  sep='\t')
    del(df_new, df)
