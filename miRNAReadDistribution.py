"""This python script is used to get the distribution of miRNAs
among the downloaded mirna related genome reads of one sample,
a miRNADistribute.tsv file will be generated for each sample

This code is used in the following format

    python miRNAReadDistribution.py

"""

import os
from os import listdir
from os.path import join

__author__ = "Yafei Xing"
__copyright__ = "Copyright 2018, AMG-OUS"
__version__ = "1.0.1"
__maintainer__ = "Yafei Xing"
__email__ = "yafei.xing@medisin.uio.no"

# mirna file location
if os.name == 'posix': # Mac
    dir_mirna = '/Users/yafeix/Work/Projects/1XGenome/Materials/hsa_miRNA_without_head.gff3'
else: #Fedora
    dir_mirna = '/home/yafei/Projects/1XGenome/Materials/hsa_miRNA_without_head.gff3'

# data folder
if os.name == 'posix': # Mac
    dir_data = '/Users/yafeix/Work/Projects/1XGenome/Data'
else:
    dir_data = '/run/media/simonrayner/\'My Book Duo\'/1XGenome/data'

# global variables
mirna_chr = []
mirna_name = []
mirna_id = []
mirna_start = []
mirna_end = []


def load_mirna(file):  # load all mirnas

    global mirna_start, mirna_chr, mirna_end, mirna_id, mirna_name
    with open(file, 'r') as fmiRNA:
        for line in fmiRNA:
            mirna_chr.append(line.split(' ')[0])
            mirna_start.append(int(line.split(' ')[1]))
            mirna_end.append(int(line.split(' ')[2]))
            mirnaInfo = line.split(' ')[3]
            ind = mirnaInfo.rfind('hsa-miR')
            ind2 = mirnaInfo.rfind(';')
            mirna_name.append(mirnaInfo[ind: ind2])
            ind = mirnaInfo.find('=')
            ind2 = mirnaInfo.find(';')
            mirna_id.append(mirnaInfo[ind+1: ind2])

for f in listdir(dir_mirna):
    # read miRNA info
    filemiRNA = join(dir_mirna, f)
    ind = f.find('_miRNA')
    chrNum = f[4:ind]
    folder = folder2write + '/' + country + '/' + sampID + '/' + chrNum
    fcreate.write('mkdir ' + folder + '\n')

load_mirna(dir_mirna)