import multiprocessing as mp
from referencemiRNA import *
import os
from os import listdir
import platform
os.system("taskset -p 0xff %d" % os.getpid())

if platform.system() == 'Darwin':  # Mac
    # mirna file location
    dir_mirna = '/Users/yafeix/Work/Projects/1XGenome/Materials/hsa_miRNA_without_head_break.gff3'
    # data folder
    dir_data = '/Users/yafeix/Work/Projects/1XGenome/Data'
    # output folder
    dir_output = '/Users/yafeix/Work/Projects/1XGenome/Analysis/mirna_coverage'
else:  # Fedora
    dir_mirna = '/home/yafei/Projects/1XGenome/Materials/hsa_miRNA_without_head_break.gff3'
    dir_data = '/run/media/yafei/My Book Duo/1XGenome/data'
    dir_output = '/home/yafei/Projects/1XGenome/Analysis/mirna_coverage'

# create files for saving the coverage of each nt in each mirna
# Head of the file: mirna sample country coverage
filename = 'Individual_coverage_mirna_break.tsv'
fwrite2 = os.path.join(dir_output, filename)
f2write = open(fwrite2, 'w')
head = 'mirnaID\tchr\tsample\tcountry\tcoverage\n'
f2write.write(head)

list_country = ['GWD', 'ESN', 'JPT', 'CHS', 'LWK', 'FIN', 'CDX', 'BEB', 'PEL', 'KHV']


def coverage_all(mirna, dire):
    """
    :type dir: directory of genome data
    """
    global dir_data, dir_output
    if set(listdir(dire)) <= set(list_country):
        # get the country code
        for country_code in listdir(dire):
            mirna.country = country_code
            # get the country folder path
            dir_country = os.path.join(dir_data, country_code)
            # go to the country folder
            coverage_per_country(mirna, dir_country)


def coverage_per_country(mirna, country):

    # obtain all the subdirectories named by sample ID
    # get all the samples ID
    list_samples = listdir(country)
    sample_folders = [os.path.join(country, x) for x in list_samples]
    # add the sample column to the dataframe

    for f in sample_folders:
        mirna.sample = os.path.basename(f)
        coverage_per_sample(mirna, f)


def coverage_per_sample(mirna, fsample):
    # print(3)
    # folders of chromosomes
    chromo_folder = os.path.join(fsample, mirna.chromo)
    coverage_per_chromo(mirna, chromo_folder)


def coverage_per_chromo(mirna, chromo):
    """
    This function searches for the corresponding .sam file in the corresponding chromosome folder
    in one sample
    :param mirna: reference mirna object containing mirna info
    :param chromo: the path of the corresponding chromosome folder in one sample of the mirna
    """
    global f2write
    sam_files = listdir(chromo)
    for file in sam_files:
        # find the corresponding download
        ind1 = file.find('MIMAT')
        ind2 = file.rfind('_')
        mirnaID = file[ind1:ind2]
        if mirna.id == mirnaID:
            # print('process sam file')
            file_path = os.path.join(chromo, file)

            mirna.coverage = coverage_per_sam(mirna, file_path)
            row = mirna.id + '\t' + mirna.chromo + '\t' + mirna.sample + '\t' + mirna.country + '\t' + str(
                mirna.coverage) + '\n'
            f2write.write(row)
        else:
            continue


def coverage_per_sam(mirna, file):
    """
    This function is for calculating the coverage of the mirna in the same file
    :param mirna: reference mirna object containing mirna info
    :param file: the .sam file corresponding to the mirna in one sample
    :return: the coverage of each nt in a vector
    """
    coverage = [0] * (mirna.end - mirna.start + 1)  # coverage for each nt in mirna
    with open(file, 'r') as fsam:
        for read in fsam.readlines():
            genome_read = GenomeReads(start=int(read.split('\t')[3]), align=read.split('\t')[9], length=101)
            # check if there is any read who length is not 101
            if len(genome_read.align) != 101:
                # print(read.split('\t')[0], len(genome_read.align))
                genome_read.length = len(genome_read.align)
            coverage = count_per_read(coverage, mirna, genome_read)
    return coverage


def count_per_read(count, mirna, read):
    """
    :type mirna: ReferencemiRNA object
    :type count: the coverage counting array
    :type read: GenomeReads object
    """
    for ind, val in enumerate(count):
        pos_ind = mirna.start + ind  # the position of the nt to be processed
        if read.start <= pos_ind <= (read.start + read.length - 1):  # if it is in the range of the read
            count[ind] = val + 1
    return count


def process_mirna(line_mirna):
    # create a mirna object
    mirnaInfo = line_mirna.split(' ')[3]
    # obtain mirna ID
    ind = mirnaInfo.find('=')
    ind2 = mirnaInfo.find(';')
    # create mirna object with current reference mirna read
    ref_mirna = ReferencemiRNA(id=mirnaInfo[ind + 1: ind2], chromo='chr' + line_mirna.split(' ')[0],
                               start=int(line_mirna.split(' ')[1]), end=int(line_mirna.split(' ')[2]),
                               coverage=[], sample='', country='')
    # calculate the coverage of each mirna in downloaded genome data
    print(ref_mirna.id)
    coverage_all(ref_mirna, dir_data)

    # delete the mirna object
    del ref_mirna


# read mirna file
with open(dir_mirna, 'r') as fmiRNA:
    pool = mp.Pool(processes=8)
    pool.map(process_mirna, fmiRNA.readlines())
    pool.close()
    pool.join()

f2write.close()
