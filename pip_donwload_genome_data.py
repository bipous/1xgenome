#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""This pipeline is used to generate a batch shell script
for downloading 1000 Genomes GRCh38 low coverage data using gff3 formatted
functional gene sequence information file as reference.
This code is used in the follwoing format

    python3 /PATH/TO/pip_download_genode_data.py -c '3utr'/'mirna'/'pri_mirna'/'pre_mirna'/'exon'/'gene'
    -t 'bam'/'sam'/'cram' -i /PATH/TO/INDEXFILE -p /PATH/TO/SAVE/BASHSCRIPT -r /PATH/TO/GFFFILE -e/-u

An example:

    sudo python3 /home/yafei/Projects/1XGenome/Codes/pip_donwload_genome_data.py -c '3utr' -t 'bam'
    -i /home/yafei/Projects/1XGenome/Materials/1000genomes.low_coverage.cram.index
    -p /home/yafei/Projects/1XGenome/Materials -r /home/yafei/Projects/1XGenome/Materials/3UTR_EMC7.gff3 -e 100000

-c to indicate the class/type of the functional gene sequence that the data related to
-t to indicate the type of data to be downloaded: bam/cram/sam
-i to indicate the index file name with path
-p to indicate the folder path for saving the download batch file and some other files
-r to indicate the reference functional gene sequence file with path
-e to indicate the region to be downloaded is an extension of the reference region or not, and for how long
    if it it not indicated, no extension will be applied

Dependencies:

bcbio-gff
"""

import argparse
import sys
import os
import logging
from BCBio import GFF


__author__ = "Yafei Xing"
__email__ = "yafei.xing@medisin.uio.no"

MY_NEWLINE = "\n"
if os.name == "Windows":
    MY_NEWLINE = "\r\n"

logging.getLogger().setLevel(logging.INFO)

parser = argparse.ArgumentParser(description='set up batch commands to download 1XGenomes data')

parser.add_argument("-c", "--classSeq", dest='classSeq',
                    help="the class/type of the functional gene sequence that the data related to")

parser.add_argument("-t", "--typeData", dest='typeData',
                    help="the type of data to be downloaded: bam/cram/sam")

parser.add_argument("-p", "--path4Batch", dest='path4Batch',
                    help="folder path for saving the download batch file and some other files")

parser.add_argument("-i", "--idxFile", dest='idxFile',
                    help="the index file name with path")

parser.add_argument("-r", "--reference", dest="reference",
                    help="the reference functional gene sequence file with path")

parser.add_argument("-H", "--HelpMe", action="store_true",
                    help="print detailed help")

parser.add_argument("-e", "--extendedLen", dest="extendedLen",
                    help="to store the downloaded data in the '~/extended' folder and for how long")

args = parser.parse_args()


def printLongHelpAndExit():
    logging.info("+" + "-" * 78 + "+")
    logging.info("+  pip_download_genome_data.py:                                                   +")
    logging.info("+    Pipeline for downloading 1X genomes data                                     +")
    logging.info("+                                                                                 +")
    logging.info("+    you need to specify:                                                         +")
    logging.info("+                                                                                 +")
    logging.info("+      the class/type of the functional gene sequence that the data related to:   +")
    logging.info("+      (-c/--classSeq)                                                            +")
    logging.info("+         it has to be one of the followings:                                     +")
    logging.info("+         mirna/3utr/pri_mirna/pre_mirna/exon/gene                                +")
    logging.info("+                                                                                 +")
    logging.info("+                                                                                 +")
    logging.info("+      the type of data to be downloaded:           (-t/--typeData)               +")
    logging.info("+         it has to be one of the followings:                                     +")
    logging.info("+            bam/cram/sam                                                         +")
    logging.info("+                                                                                 +")
    logging.info("+                                                                                 +")
    logging.info("+      folder path for saving the download batch file and some other files:       +")
    logging.info("+      (-p/--path4Batch)                                                          +")
    logging.info("+         it has to be in the format of /ABSOLUTE/PATH/ or ./RELEVANT/PATH/       +")
    logging.info("+                                                                                 +")
    logging.info("+                                                                                 +")
    logging.info("+      the index file name with path:                 (-i/--idxFile)              +")
    logging.info("+         The file is for indexing the samples in each population;                +")
    logging.info("+         it has to be in the format of /ABSOLUTE/PATH/TO/FILENAME or             +")
    logging.info("+         ./RELEVANT/PATH/TO/FILE                                                 +")
    logging.info("+                                                                                 +")
    logging.info("+                                                                                 +")
    logging.info("+      the reference functional gene sequence file with path:   (-r/--reference)  +")
    logging.info("+         The file is a reference for the ID, name, and locations                 +")
    logging.info("+         for the wanted functional gene region                                   +")
    logging.info("+         it has to be in the format of /ABSOLUTE/PATH/TO/FILENAME or             +")
    logging.info("+         ./RELEVANT/PATH/TO/FILE                                                 +")
    logging.info("+                                                                                 +")
    logging.info("+                                                                                 +")
    logging.info("+      define the download region:                    (-e)                        +")
    logging.info("+        -e, the extended length of the region to be downloaded                   +")
    logging.info("+                                                                                 +")
    logging.info("+                                                                                 +")
    logging.info("+" + "-" * 78 + "+")


def printHelpAndExit():
    parser.print_help()
    logging.info("stopping")
    sys.exit()


def checkLength():
    logging.info('check the length of data to be downloaded:')
    if args.extendedLen:
        if int(args.extendedLen)<0:
            logging.error('---the length for extending the download region should be positive')
            logging.info("--stopping")
            sys.exit()
        elif int(args.extendedLen) >=0:
            args.extendedLen = int(args.extendedLen)
            logging.info("--ok")
    else:
        logging.info("--non-extended data will be downloaded")



def checkRefFile(pyscriptFolder):
    logging.info('check the existence of the specified reference file:')

    if args.reference:
        args.reference = args.reference.strip('.')
        if not os.path.isfile(args.reference):
            args.reference = os.path.join(pyscriptFolder, args.reference)
            logging.info("--OK")

        if not os.path.isfile(args.reference):
            logging.error("--the file doesn't exist, can't continue. Try checking the specified path or the file name")
            logging.info("--stopping")
            sys.exit()
    else:
        logging.error("----you need to specify a reference file")
        printHelpAndExit()


def checkIndexFile(pyscriptFolder):
    logging.info('check the existence of the specified index file:')

    if args.idxFile:
        args.idxFile = args.idxFile.strip('.')
        if not os.path.isfile(args.idxFile):
            args.idxFile = os.path.join(pyscriptFolder, args.idxFile)
            logging.info("--OK")

        if not os.path.isfile(args.idxFile):
            logging.error("--the file doesn't exist, can't continue. Try checking the specified path or the file name")
            logging.info("--stopping")
            sys.exit()
    else:
        logging.error("----you need to specify an index file")
        printHelpAndExit()



def checkScriptFolder(pyscriptFolder):
    logging.info('check the existence of the folder to write the batch script')
    global fDownload, fCreate

    if args.path4Batch:
        args.path4Batch = args.path4Batch.strip('.')
        # is the file path a relative path (does it start with a '.' ?)
        if not os.path.isdir(args.path4Batch):
            args.path4Batch = os.path.join(pyscriptFolder, args.path4Batch)

        if not os.path.isdir(args.path4Batch):
            logging.error("--the folder doesn't exist, can't continue. Try checking the specified path")
            logging.info("--stopping")
            sys.exit()
        else:
            fDownload = os.path.join(args.path4Batch, fDownload)
            fCreate = os.path.join(args.path4Batch, fCreate)
            logging.info("--OK")
    else:
        logging.error("----you need to specify an output folder using the -o/--outFolder parameter")
        printHelpAndExit()


def checkDataType():
    logging.info('checking the format of the data to download:')

    if args.typeData:
        if args.typeData in DataFormat:
            logging.info("--OK")
        else:
            logging.error('----you need to specify the format of the data among the formats of .bam, .sam and .cram')
            printHelpAndExit()
    else:
        logging.error('----you need to specify the format of the data among the formats of .bam, .sam and .cram')
        printHelpAndExit()


def checkSequenceType():
    logging.info('checking the type of the functional gene sequence:')

    if args.classSeq:
        if args.classSeq in SeqType:

            logging.info("--OK")
        else:
            logging.error('----you need to specify the type of the sequence among mirna, 3utr, pri_mirna, pre_mirna, '
                          'exon and gene')
            printHelpAndExit()
    else:
        logging.error('----you need to specify the type of the sequence among mirna, 3utr, pri_mirna, pre_mirna, '
                      'exon and gene')
        printHelpAndExit()


def createFolder(subdir):
    global outPath
    logging.info('checking the existence of the main directory for storing data')

    outPath = os.path.join(outPath, subdir)
    if not os.path.isdir(outPath):
        folderFile.write('mkdir ' + outPath + '\n')
        logging.info('the directory %s does not exist, written to createFolder.sh file', outPath)
    else:
        logging.info('the directory %s exists', outPath)


def checkArgs():
    if args.HelpMe:
        printLongHelpAndExit()
        exit()
    pyscriptFolder = os.path.dirname(os.path.abspath(__file__))  # current absolute path
    checkScriptFolder(pyscriptFolder)
    checkDataType()
    checkSequenceType()
    checkIndexFile(pyscriptFolder)
    checkRefFile(pyscriptFolder)
    checkLength()


def get_identification(classSeq,feature):
    way2name= tags[classSeq]
    names = []
    if classSeq == 'mirna':
        for way in way2name:
            names.append(feature.qualifiers[way][0])
        id = '_'.join(names)
    elif classSeq == '3utr':
        if feature.type == 'inferred_parent': #combined regions
            id_temp = feature.qualifiers[way2name[0]][0]
        elif feature.type == dict_gff_type[classSeq]: #single region
            id_temp = feature.qualifiers[way2name[1]][0]
        pos = id_temp.find(':')
        id = id_temp[pos+1:len(id_temp)]
    return id


def get_gff_query_info(feature):
    seqID = get_identification(args.classSeq, feature)  # get the unique sequence ID
    if args.extendedLen:
        start = feature.location.nofuzzy_start - args.extendedLen + 1
        if start < 0:
            start = 1
        end = feature.location.nofuzzy_end + args.extendedLen
    else:
        start = feature.location.nofuzzy_start + 1
        end = feature.location.nofuzzy_end
    return seqID, start, end


def writeScript():
    global outPath, downloadFile, folderFile
    logging.info('start to generate download batch script')

    ftp = 'ftp:/ftp.1000genomes.ebi.ac.uk/vol1/ftp/data_collections/1000_genomes_project/data/'
    len_ftp = len(ftp)
    list_country = ['LWK', 'FIN', 'CDX', 'BEB', 'PEL', 'KHV', 'GWD', 'ESN', 'JPT', 'CHS']

    # set filter for the type of sequence
    filter_info = dict(gff_type=[dict_gff_type[args.classSeq]])

    # process each query in the filtered reference file
    with open(args.reference,'r') as fRef:
        # read the reference file to get the sequence information: chromosome, location, name, id
        for rec in GFF.parse(fRef, limit_info=filter_info):
            for i in range(len(rec.features)):
                chrNum = rec.id  # normally chromosome id
                if 'chr' not in chrNum and 'Chr' not in chrNum:
                    chrNum = 'chr'+chrNum
                seqID, start, end = get_gff_query_info(rec.features[i])

                # set the download folder by the unique name of the sequence
                folder = outPath + '/' + seqID
                if not os.path.isdir(folder):
                    folderFile.write('mkdir ' + folder + '\n')

                # process each related index for downloading
                list_country_temp = []
                with open(args.idxFile, 'r') as dIndex:
                    for ftp_addr in dIndex:
                        ind = ftp_addr.rfind('/')
                        ind_end = ftp_addr.find('.cram')
                        country = ftp_addr[len_ftp:len_ftp + 3]

                        if country in list_country:
                            # set the download folder by the population name
                            folder = outPath + '/' + seqID + '/' +country
                            if not os.path.isdir(folder) and country not in list_country_temp:
                                folderFile.write('mkdir ' + folder + '\n')
                                list_country_temp.append(country)

                            # set the name of download file
                            filename = ftp_addr[ind:ind_end] + "." + args.typeData
                            # generate the command

                            downloadFile.write(
                                "samtools view " + samtools_option[args.typeData] + " " + ftp_addr.rstrip() + " " + chrNum + ":" + str(start) + "-" + str(
                                    end) + " -o " + folder + filename + '\n')
                            # generate the index file for bam files
                            if args.typeData == 'bam':
                                createIndex.write(
                                   'samtools index ' + folder +filename + '\n')


# global variables
SeqType = ['mirna','3utr','pri_mirna','pre_mirna','exon','gene']
dict_gff_type = {'mirna': 'miRNA', '3utr': 'three_prime_UTR', 'pri_mirna': 'miRNA_primary_transcript'}
DataFormat = ['bam','sam','cram']
tags = {'mirna':['ID','Name'], '3utr':['ID','Parent']}
samtools_option = {'bam':'-bh','sam':'-h', 'cram':'-C'}
outPath = '/media/MyBookDuo/1XGenome/'
fDownload = 'downBatch.sh'
fCreate = 'createFolder.sh'

# start process
checkArgs()

downloadFile = open(fDownload,'w')
folderFile = open(fCreate, 'w')
if args.typeData == 'bam':
    createIndex = open(os.path.join(args.path4Batch, 'createIndex.sh'), 'w')

createFolder(args.typeData)
createFolder(args.classSeq)

writeScript()

downloadFile.close()
folderFile.close()
if args.typeData == 'bam':
    createIndex.close()



