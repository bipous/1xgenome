#!/bin/bash
# if [ ! -d "/media/MyBookDuo/1XGenome/bam_data" ]; then
# 	mkdir "/media/MyBookDuo/1XGenome/bam_data"
# fi

 for dircountry in /media/MyBookDuo/1XGenome/data/*/ # list directories 
 do
# 	#echo $dir
 	dir=${dircountry%*/}	# remove the trailing "/"
 	country=${dir##*/}	# print everything after the final "/"
 	echo $country
# 	if  [ ! -d "/media/MyBookDuo/1XGenome/bam_data/"$country ]; then
# 		mkdir "/media/MyBookDuo/1XGenome/bam_data/"$country
# 	fi
 	for dirsample in $dircountry*/
 	do
 		dir=${dirsample%*/}
 		sample=${dir##*/}
# 		if [ ! -d "/media/MyBookDuo/1XGenome/bam_data/"$country"/"$sample ]; then
#			mkdir "/media/MyBookDuo/1XGenome/bam_data/"$country"/"$sample
#		fi
# 		#echo $dir1
 		for dirchromo in $dirsample*/
 		do
 			dir=${dirchromo%*/}
 			chromo=${dir##*/}
# 			if [ ! -d "/media/MyBookDuo/1XGenome/bam_data/"$country"/"$sample"/"$chromo ]; then
# 				mkdir "/media/MyBookDuo/1XGenome/bam_data/"$country"/"$sample"/"$chromo
# 			fi
			for file in $dirchromo*".sam"
			do
				cat $dirsample*"_Head.sam" "$file" > "/home/yafei/Projects/1XGenome/Data/temp.sam"
				samtools view -b "/home/yafei/Projects/1XGenome/Data/temp.sam" > "/media/MyBookDuo/1XGenome/bam_data/"$country"/"$sample"/"$chromo"/"$(basename "$file" .sam)".bam"
				samtools index "/media/MyBookDuo/1XGenome/bam_data/"$country"/"$sample"/"$chromo"/"$(basename "$file" .sam)".bam"
#				echo $file
			done
 		done
 	done
 done

#for file in /Users/yafeix/Work/Projects/1XGenome/Data/chr1/*.sam;
#do
#	cat "/Users/yafeix/Work/Projects/1XGenome/Data/1XGenomes_GRCh38_lowcov_LWK_NA19017_Head.sam" "$file" > "/Users/yafeix/Work/Projects/1XGenome/Data/temp.sam"
#	samtools view -b "/Users/yafeix/Work/Projects/1XGenome/Data/temp.sam" > "/Users/yafeix/Work/Projects/1XGenome/Data/$(basename "$file" .sam).bam"
#done