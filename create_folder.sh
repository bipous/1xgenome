#!/usr/bin/env bash
folder="/media/MyBookDuo/1XGenome/miR_only_bam_data/"
file="/home/yafei/Projects/1XGenome/Materials/hsa_miRNA_ID_name.txt"
#file="/Users/yafeix/Work/Projects/1XGenome/Materials/hsa_miRNA_ID_name.txt"

while IFS= read -r findex
do
    for i in 'GWD' 'ESN' 'JPT' 'CHS' 'LWK' 'FIN' 'CDX' 'BEB' 'PEL' 'KHV'
    do
	    mkdir $folder$findex'/'$i
	done
done <"$file"