#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""This python script is used to generate a batch shell script 
for downloading 1000 Genomes GRCh38 low coverage data

This code is used in the follwoing format

    python GenomeDownload.py

"""

import argparse
import sys
import os
import logging
import datetime
from os import listdir
from os.path import isfile, join


__author__ = "Yafei Xing"
__copyright__ = "Copyright 2018, AMG-OUS"
__version__ = "1.0.1"
__maintainer__ = "Yafei Xing"
__email__ = "yafei.xing@medisin.uio.no"
__status__ = "Production"

#low coverage data index file
dir_cram_index = "/home/yafei/Projects/1XGenome/Materials/1000genomes.low_coverage.cram.index"
#main fpt address
ftp = 'ftp:/ftp.1000genomes.ebi.ac.uk/vol1/ftp/data_collections/1000_genomes_project/data/'
len_ftp = len(ftp)
#download locations according to mirna locations
dir_mirna = '/home/yafei/Projects/1XGenome/Materials/hsa_miRNA/'
#list of countries, samples of which will be downloaded
list_country = ['LWK', 'FIN', 'CDX', 'BEB', 'PEL']#['KHV'] ['GWD', 'ESN', 'JPT', 'CHS']#
#directory for downloaded data
folder2write_linux = '/run/media/simonrayner/My Book Duo/1XGenome/data'
folder2write='/run/media/simonrayner/\'My Book Duo\'/1XGenome/data'
#bash script for download command, to be used by GNU parallel
fwrite = open('/home/yafei/Projects/1XGenome/Materials/downloadBatch.sh','w')
#create dictories according to the data features
fcreate = open('/home/yafei/Projects/1XGenome/Materials/createFolder.sh','w')

if not os.path.isdir(folder2write_linux):
	fcreate.write('mkdir '+folder2write+'\n')
for country in list_country:
	if not os.path.isdir(folder2write_linux+'/'+country):
		fcreate.write('mkdir '+folder2write+'/'+country+'\n')

#command example:
#mkdir /data/country/sampleID/chrNum
#samtools view -h 
#ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/data_collections/1000_genomes_project/data/ACB/HG01879/high_cov_alignment/HG01879.alt_bwamem_GRCh38DH.20150917.ACB.high_coverage.cram 
#chr17:7512445-7513455 -o /1Xdata/country/sampleID/chrNum/1XGenomes_GRCh38_lowcov_country_sampleID_chrNum_mirName.sam

#read cram files index

with open (dir_cram_index,'r') as fIndex:
	for ftp_addr in fIndex:
		country = ftp_addr[len_ftp:len_ftp+3]
		if country in list_country:				
			sampID = ftp_addr[len_ftp+4:len_ftp+11]
			if not os.path.isdir(folder2write_linux+'/'+country+'/'+sampID):
				fcreate.write('mkdir '+folder2write+'/'+country+'/'+sampID+'\n')
			folder = folder2write+'/'+country+'/'+sampID
			filename = "/1XGenomes_GRCh38_lowcov_"+country+"_"+sampID+"_Head.sam"
			fwrite.write("samtools view -H "+ftp_addr.rstrip()+" -o "+folder+filename+'\n')
			#print country, sampID
			for f in listdir(dir_mirna):
				#read miRNA info
				filemiRNA = join(dir_mirna,f)
				ind = f.find('_miRNA')
				chrNum = f[4:ind]
				folder = folder2write+'/'+country+'/'+sampID+'/'+chrNum
				fcreate.write('mkdir '+folder+'\n')


				with open(filemiRNA,'r') as fmiRNA:
					for line in fmiRNA:
						start = int(line.split(' ')[1])-10000
						if start < 0:
							start = 1
						end = int(line.split(' ')[2]) + 10000
						info = line.split(' ')[3]
						ind = info.rfind('hsa-miR')
						ind2 = info.rfind(';')
						name = info[ind: ind2]
						ind = info.find('=')
						ind2 = info.find(';')
						RnaID = info[ind+1: ind2]
						filename = "/1XGenomes_GRCh38_lowcov_"+country+"_"+sampID+"_"+chrNum+"_"+RnaID+"_"+name+".sam"
						fwrite.write("samtools view "+ftp_addr.rstrip()+" "+chrNum+":"+str(start)+"-"+str(end)+" -o "+folder+filename+'\n')
						#print start, end, name

fwrite.close()
fcreate.close()
