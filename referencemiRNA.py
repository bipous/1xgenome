"""
This python script defines the class of reference miRNA

It includes the following attributes:
    ID: ID of the mirna
    name: name of the mirna
    chr: the chromosome where the mirna locates
    start: starting point of the mirna sequence
    end: ending point of the mirna sequence

"""

import unittest
import pandas as pd


class ReferencemiRNA:

    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)

    def create_dataFrames(self, list_of_names):
        for name in list_of_names:
            exec('self.' + name + ' = pd.DataFrame()')

class GenomeReads:

    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)

class TestReferencemiRNA(unittest.TestCase):

    def test_init(self):
        a = 'MIMAT0027618'
        b = 1
        c = 17409
        d = 17431
        rna = ReferencemiRNA(id=a, chromo=b, start=c, end=d)
        self.assertEqual(rna.id,'MIMAT0027618')
        self.assertEqual(rna.chromo, 1)
        self.assertEqual(rna.start, 17409)
        self.assertEqual(rna.end, 17431)

class TestGenomeReads(unittest.TestCase):

    def test_init(self):
        read = GenomeReads(country='CHS', chromo='chr1', start=156410269)
        self.assertEqual(read.country, 'CHS')
        self.assertEqual(read.chromo, 'chr1')
        self.assertEqual(read.start, 156410269)
